<?php

namespace Drupal\flag_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\flag\Entity\Flag;
use Drupal\flag\FlagLinkBuilderInterface;
use Drupal\flag\FlagServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Flag Block.
 *
 * @Block(
 *   id = "flag_block",
 *   admin_label = @Translation("Flag block"),
 *   category = @Translation("Flag")
 * )
 */
class FlagBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  private $flag;

  /**
   * Flag link builder service.
   *
   * @var \Drupal\flag\FlagLinkBuilderInterface
   */
  private $flagLinkBuilder;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * FlagBlock constructor.
   *
   * @param array $configuration
   *   The configuration array for the block.
   * @param string $plugin_id
   *   The plugin ID of the block.
   * @param mixed $plugin_definition
   *   The plugin definition of the block.
   * @param \Drupal\flag\FlagServiceInterface $flag
   *   The flag service used for flag operations.
   * @param \Drupal\flag\FlagLinkBuilderInterface $flag_link_builder
   *   The flag link builder service used to build flag links.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service for accessing route information.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FlagServiceInterface $flag, FlagLinkBuilderInterface $flag_link_builder, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->flag = $flag;
    $this->flagLinkBuilder = $flag_link_builder;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('flag'),
      $container->get('flag.link_builder'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['flag_block_settings'] = [
      '#type' => 'select',
      '#title' => $this->t('Flag'),
      '#options' => $this->getFlagTypes(),
      '#required' => TRUE,
      '#default_value' => $config['flag_block_settings'] ?? '',
      '#description' => $this->t('You need select a type of <a href="@flag" target="_blank">Flag</a>', [
        '@flag' => Url::fromRoute('entity.flag.collection')->toString(),
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['flag_block_settings'] = $values['flag_block_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): ?array {
    $config = $this->getConfiguration();
    /** @var \Drupal\flag\Entity\Flag $flag */
    $flag = $this->flag->getFlagById($config['flag_block_settings']);
    if ($entity = $this->getRouteEntity()) {
      if (!$this->checkBlockRender($flag, $entity)) {
        return NULL;
      }
      $build = $this->flagLinkBuilder->build(
          $flag->getFlaggableEntityTypeId(),
          $entity->id(),
          $config['flag_block_settings']
        );
      return [
        'flag' => $build,
      ];
    }
    else {
      return NULL;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Check possibility of flag render.
   *
   * @param \Drupal\flag\Entity\Flag $flag
   *   Flag entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity from route.
   *
   * @return bool
   *   Render or not block content.
   */
  private function checkBlockRender(Flag $flag, ContentEntityInterface $entity): bool {
    // Compare flag and entity type ID.
    if ($flag->getFlaggableEntityTypeId() !== $entity->getEntityTypeId()) {
      return FALSE;
    }
    // Compare flag and entity bundles.
    $flag_bundles = $flag->getBundles();
    $entity_bundle = $entity->bundle();
    if (!empty($flag_bundles) && !empty($entity_bundle) && !in_array($entity_bundle, $flag_bundles)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get all flag types.
   *
   * @return array
   *   Flag types array.
   */
  private function getFlagTypes(): array {
    $flags = $this->flag->getAllFlags();
    $flag_list = [];

    foreach ($flags as $flag) {
      $flag_list[$flag->id()] = $flag->label();
    }

    return $flag_list;
  }

  /**
   * Get entity ID from path.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The content entity corresponding to the entity ID extracted
   *   from the path, *   or null if no entity ID is found.
   */
  private function getRouteEntity(): ?ContentEntityInterface {
    // Entity will be found in the route parameters.
    if (($route = $this->routeMatch->getRouteObject()) && ($parameters = $route->getOption('parameters'))) {
      // Determine if the current route represents an entity.
      foreach ($parameters as $name => $options) {
        if (isset($options['type']) && strpos($options['type'], 'entity:') === 0) {
          $entity = $this->routeMatch->getParameter($name);
          // Check if it is a correct entity.
          if ($entity instanceof ContentEntityInterface && $entity->hasLinkTemplate('canonical')) {
            return $entity;
          }
        }
      }
    }
    return NULL;
  }

}
