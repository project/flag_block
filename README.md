# Flag Block

This is a very simple module makes available Flag link in a block.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/flag_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/flag_block).


## Requirements

- Block module (core)
- [Flag module](https://www.drupal.org/project/flag)


## Installation

If your site is managed via Composer, use Composer to download the module.

`composer require drupal/flag_block:^1.0`

Or

Install the module as any other module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module in 'Extend' page (/admin/modules).

   Or

   use Drush: `drush en flag_block`

2. Visit the 'Block layout' page and click 'Place block'.
3. On the block settings for you will see 'Flag' field. Select one from the
   your Flag types list (`/admin/structure/flags`).
4. Click 'Save block'.
